from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
import numpy as np
import pandas as pd


def load_text(path=''):
    """
    input path to a table with the queries that we want to classify
    :return: two lists: one corpus and one vocabulary
    """
    table = pd.read_csv(path, usecols=['Meta Description 1', '2_level 1'])  # feature, label
    # number_of_classes = table['2_level 1'].nunique()
    # dict1 = dict(zip(table['2_level 1'].unique().tolist(), range(0, number_of_classes)))  # converts labels to numbers
    # not needed for fast text
    # table['labels'] = table['2_level 1'].map(dict1, na_action='ignore')
    # table = table.drop(columns=['2_level 1'])
    table.columns = ['feature', 'label']
    return corpus, vocabulary


# some example corpus
corpus = ['this is the first document',
          'this document is the second document',
          'and this is the third one',
          'is this the first document']

# some example vocab
vocabulary = ['this', 'document', 'first', 'is', 'second', 'the',
              'and', 'one']


pipe = Pipeline([('count', CountVectorizer(vocabulary=vocabulary)),
                 ('tfid', TfidfTransformer())]).fit(corpus)
pipe['count'].transform(corpus).toarray()


if __name__ == '__main__':
    pass