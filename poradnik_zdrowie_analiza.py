import pandas as pd
import string
import httpx
from googletrans import Translator
import random
import fasttext

timeout = httpx.Timeout(60)
translator = Translator(timeout=timeout)


def translate(x):
    """translates polish to english and then back again from english to polish"""
    tl = translator.translate(x, src='pl')
    return translator.translate(tl.text, dest='pl').text

table = pd.read_csv('/mnt/sd1/branza_medyczna/branza_medyczna_przetworzone_pliki_poradnik_zdrowie.csv')

table = table.dropna()
table['title_translated'] = table.title.apply(lambda x: translate(str(x)))
table.to_csv('/mnt/sd1/branza_medyczna/branza_medyczna_przetworzone_pliki_poradnik_zdrowie_translation_augmented.csv',index=False)

# make a fast text augmentation for the translated and original titles
table_original = table[['title', 'fourth_element']]
table_original.columns = ['title', 'label']

table_translation_augmented = table[['title_translated', 'fourth_element']]
table_translation_augmented.columns = ['title', 'label']

new_table = pd.concat([table_original, table_translation_augmented], axis=0)

polish_stopwords = pd.read_csv('/home/erazer/PycharmProjects/jigsaw/nlp_utils/polish_stopwords.txt', header=None,
                               names=['words']).words.to_list()


def remove_stopwords(x, polish_stopwords):
    cleaned = [elem for elem in x if elem not in polish_stopwords]
    cleaned = [elem for elem in cleaned if elem not in string.punctuation]
    cleaned = [elem for elem in cleaned if not elem.isdigit()]
    return " ".join(cleaned)


def replace_with_synonyms(x, ft, percent_to_change=0.5):
    """takes a pretrained model and replaces every word in undersampled classes with the word lying the closest, based
    on fasttext"""

    # ft.get_nearest_neighbors('cześć')

    how_many_words = round(len(x.split(" ")) * percent_to_change) + 1
    # get random indices of the words to change with a synonym
    synonyms_indices = random.sample(range(len(x.split(" "))), how_many_words)
    sentence = x.split(" ")
    for elem in synonyms_indices:
        try:
            sentence[elem] = ft.get_nearest_neighbors(sentence[elem])[0][1]
        except:
            sentence[elem] = sentence[elem]
            print("something went wrong, leaving the original element")
    return ' '.join(sentence)


new_table['title_no_stopwords'] = new_table.title.apply(lambda x: remove_stopwords(str(x).split(' '), polish_stopwords))

data_to_augment = new_table.loc[(new_table.label == 'hormony') |
                                (new_table.label == 'grypa i przeziebienie') |
                                (new_table.label == 'uklad krwionosny') |
                                (new_table.label == 'apteczka') |
                                (new_table.label == 'uklad nerwowy') |
                                (new_table.label == 'psychiatria') |
                                (new_table.label == 'metody alternatywne') |
                                (new_table.label == 'nowotwory') |
                                (new_table.label == 'choroby meskie') |
                                (new_table.label == 'uklad pokarmowy')
                                ]
path_to_model = '/mnt/sd1/pretrained_models/cc.pl.300.bin'
ft = fasttext.load_model(path_to_model)
data_to_augment['ft_augmented'] = data_to_augment['title_no_stopwords'].apply(lambda x: replace_with_synonyms(x, ft, percent_to_change=0.5))
data_to_augment.to_csv('/mnt/sd1/branza_medyczna/branza_medyczna_przetworzone_pliki_poradnik_zdrowie_fasttext_augmented.csv', index=False)

# for classification

table = pd.read_csv('/mnt/sd1/branza_medyczna/poradnik_zdrowie/branza_medyczna_przetworzone_pliki_poradnik_zdrowie.csv')
table_translation_aumgented = pd.read_csv('/mnt/sd1/branza_medyczna/poradnik_zdrowie/branza_medyczna_przetworzone_pliki_poradnik_zdrowie_translation_augmented.csv')
table_ft_augmented = pd.read_csv('/mnt/sd1/branza_medyczna/poradnik_zdrowie/branza_medyczna_przetworzone_pliki_poradnik_zdrowie_fasttext_augmented.csv')

# take the values from table table_translation_aumgented and stack them concatenate them,m so there is more data.
# That table has all labels augmented.

table_with_translation = table_translation_aumgented[['fourth_element', 'title_translated']]
table_with_translation.columns = ['label', 'title']

table = table[['fourth_element', 'title']]
table.columns = ['label', 'title']



table_ft_augmented = table_ft_augmented[['ft_augmented', 'label']]
table_ft_augmented.columns = ['title', 'label']
combine_all_data = pd.concat([table, table_with_translation, table_ft_augmented],axis=0)


def cut_too_many(group, k=5000):
    if len(group) < k:
        return group
    else:
        return group.sample(k)


def label_to_number(table):
    num_of_elements = table.label.nunique()
    dict_ = dict(zip(table.label.unique().tolist(), range(num_of_elements)))
    table['label_numerical'] = table.label.map(dict_)
    return table

combine_all_data = label_to_number(combine_all_data)

combine_all_data = combine_all_data.dropna()

balanced = combine_all_data.groupby('label').apply(cut_too_many).reset_index(drop=True)
table_finanse = balanced.copy()

def _make_int(x):
    return int(x)

table_finanse.label_numerical = table_finanse.label_numerical.apply(_make_int)

def check_lengths(x):
    return len(x.split(' '))

table_finanse['length'] = table_finanse.title.apply(check_lengths)

# make data for classification



