""" this script uses a fast text facebook implementation, and gives an astounding results of .94 accuracy on the stemmed
dataset"""

import pandas as pd
from sklearn.utils import shuffle
import fasttext


def append_prefix(x):
    prefix = '__label__'
    return prefix + str(x)


if __name__ == '__main__':

    # read the data
    table_string_label_stemmmed = pd.read_csv('/home/mlewandowski/Downloads/klasyfikacja teksty/stemmed_test_train.csv',
                                              usecols=['new_columns', 'label'])

    # add prefix __label__ to label column and drop the original label column
    table_string_label_stemmmed['final_label'] = table_string_label_stemmmed.label.apply(lambda x: append_prefix(x))
    table_string_label_stemmmed = table_string_label_stemmmed.drop(columns=['label'])

    # merge label and title to one column
    table_string_label_stemmmed["column_merged"] = table_string_label_stemmmed.apply(lambda x: ' '.join(x.astype(str)), axis=1)

    # make a train test split
    merged = shuffle(table_string_label_stemmmed["column_merged"])
    train_split = merged.iloc[:int(merged.shape[0] * 0.8)]
    test_split = merged.iloc[int(merged.shape[0] * 0.8):]

    # write it on a drive to get rid of index and header for fasttext classifier
    train_split.to_csv('table_train', index=False, header=None)
    test_split.to_csv('table_test', index=False, header=None)

    # as a path use paths resulting from the above version
    model = fasttext.train_supervised(input='/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/table_train',
                                      autotuneValidationFile='/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/table_test')

    print(model.test('/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/table_test'))




