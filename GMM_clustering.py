from sklearn.mixture import GaussianMixture, BayesianGaussianMixture
from sklearn.utils import shuffle
import pandas as pd
import matplotlib.pyplot as plt
from glove_embedding import get_word_embedding
from utils import average_sequences
from gensim.models import KeyedVectors

path_merged = '/home/mlewandowski/Downloads/klasyfikacja teksty/merged.csv'
table_original = pd.read_csv(path_merged)
table = table_original.copy()
model_word2vec = KeyedVectors.load('/home/mlewandowski/Downloads/pretrained_models/word2vec/word2vec_100_3_polish.bin')
table['new'] = table.new_columns.apply(lambda x: get_word_embedding(x, model_word2vec))
table['averaged'] = table.new.apply(lambda x: average_sequences(x))

table = table.drop(columns=['new_columns', 'new'])
merged = shuffle(table)
train_split = merged.iloc[:int(merged.shape[0] * 0.8), :]
test_split = merged.iloc[int(merged.shape[0] * 0.8):, :]

gmm = BayesianGaussianMixture(n_components=20, covariance_type='full').fit(train_split.averaged.to_list())
Gaussian = GaussianMixture(n_components=20).fit(train_split.averaged.to_list())
labels = gmm.predict(test_split.averaged.to_list())
labels = Gaussian.predict(test_split.averaged.to_list())
(labels == test_split.labels).sum()
labels_gaussian = gmm.predict(test_split.averaged.to_list())




