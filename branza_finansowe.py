import pandas as pd
import numpy as np
from sklearn.utils import shuffle
import fasttext


def append_prefix(x):
    prefix = '__label__'
    return prefix + str(x)


def how_many_slashes(x):
    return len(x.split("/"))


def eliminate_ending(x):
    """most of the strings are uncleaned and hence the need to eliminate """
    if x[-1] == 'wiedzieć':
        return x[:-2]
    else:
        return x


def change_to_list(x):
    """ text is represented as a string, in order to clean it and get rid of two ending words like 'warta', 'wiedzieć'
    needs to be represented as a list first and then concatenated again"""
    return x.lstrip('[').rstrip(']').replace("'", "").split(", ")


def join_list(x):
    """to have each row in the string format"""
    return ' '.join(x)


def eliminate_low_frequency_words(x, n=10):
    """eliminates words that occur less than n times, goes row by row"""
    pass


if __name__ == '__main__':

    finanse = pd.read_csv('/mnt/sd1/branza_finansowa_buisnessinsider-categories+titles.csv')
    # finanse = pd.read_csv('/mnt/sd1/branza_finansowa_forbes-categories+titles.csv')
    #
    # finanse = pd.concat([table_finanse_bi, table_finanse_forbes], axis=0)

    finanse['slashes'] = finanse.links.apply(lambda x: how_many_slashes(x))

    interm = finanse.query('6<=slashes<=7').reset_index(
        drop=True)  # select values from pandas having mentioned conditions
    indeces = finanse.query('6<=slashes<=7').index  # find the indexes of places where there is a label
    listaa = [interm.links[x].split('/')[3] for x in range(interm.shape[0])]  # get the labels (split the string
    # and take 3rd position)
    listaa = pd.DataFrame(listaa).set_index(indeces)  # add the indices to labels
    selected = finanse['title'].iloc[indeces]  # select only text column that we have an interest in
    table_with_data = pd.concat([selected, listaa], axis=1)  # concat features with labels
    table_with_data.columns = ['title', 'label']  # rename the columns

    # there are extremely few (between 1 and 6) instances of that classes, hence choosing to skip them BUSINESS INSIDER
    table_with_data = table_with_data[table_with_data.label != 'international']
    table_with_data = table_with_data[table_with_data.label != 'o-nas']
    table_with_data = table_with_data[table_with_data.label != 'waluty']
    table_with_data = table_with_data[table_with_data.label != 'zarobki-w-przedwojennej-warszawie-ile-zarabialo-sie-przed-wojna']
    table_with_data = table_with_data[table_with_data.label != 'raport-ipcc-nt-zmian-klimatu-najwazniejsze-informacje']


    # for cleaning the article from the last two words
    table_with_data['new_cleaned'] = table_with_data.cleaned_article.apply(lambda x: change_to_list(x))
    table_with_data['new_cleaned'] = table_with_data.new_cleaned.apply(lambda x: eliminate_ending(x))
    table_with_data['new_cleaned'] = table_with_data.new_cleaned.apply(lambda x: join_list(x))

    # now the data is ready for classification

    # add prefix __label__ to label column and drop the original label column
    table_with_data['final_label'] = table_with_data.label.apply(lambda x: append_prefix(x))
    table_with_data = table_with_data.drop(columns=['label'])

    table_with_data = table_with_data[table_with_data.columns[::-1]]

    # merge label and title to one column
    table_with_data["column_merged"] = table_with_data.apply(lambda x: ' '.join(x.astype(str)), axis=1)

    # make a train test split
    merged = shuffle(table_with_data["column_merged"])
    train_split = merged.iloc[:int(merged.shape[0] * 0.8)]
    test_split = merged.iloc[int(merged.shape[0] * 0.8):]

    # write it on a drive to get rid of index and header for fasttext classifier
    train_split.to_csv('table_train_finance.csv', index=False, header=None)
    test_split.to_csv('table_test_finance.csv', index=False, header=None)

    # as a path use paths resulting from the above version
    model = fasttext.train_supervised(input='table_train_finance.csv',
                                      autotuneValidationFile='table_test_finance.csv')

    print(model.test('table_test_finance.csv'))
    # (8371, 0.5913272010512484, 0.5913272010512484)  # but that is for articles, not titles

    # (7056, 0.48356009070294786, 0.48356009070294786) for titles (correctly prepared dataset) business insider

    # title of the articles (business insider)
    # finanse
    # wiadomosci
    # firmy
    # technologie
    # twoje - pieniadze
    # gielda
    # lifestyle
    # media
    # polityka
    # rozwoj - osobisty
    # motoryzacja
    # biznesvscovid
    # sport
    # wideo
    # strategie

    # for forbes:


    # each of both servers have different names for classes business insider has the following,
    # forbes has the following:






