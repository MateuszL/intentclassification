import torch
import pandas as pd
from transformers import BertForSequenceClassification, BertTokenizerFast, AdamW, get_linear_schedule_with_warmup
from torch.utils.data import TensorDataset, random_split, DataLoader, RandomSampler, SequentialSampler
import random
import numpy as np
import time
import datetime
import os


def check_cuda_availability():
    if torch.cuda.is_available():
        device = torch.device("cuda")

        print(f"Rejoice, we have a GPU {torch.cuda.device_count()}, {torch.cuda.get_device_name(0)}")

    else:
        print("shit happened, CPU only")
        device = torch.device("cpu")
    return device


def flat_accuracy(preds, labels):
    pred_flat = np.argmax(preds, axis=1).flatten()
    labels_flat = labels.flatten()
    return np.sum(pred_flat == labels_flat) / len(labels_flat)


def format_time(elapsed):
    elapsed_rounded = int(round(elapsed))
    return str(datetime.timedelta(seconds=elapsed_rounded))


class Train_Model:

    def __init__(self, epochs, tokens, classes, model_path, path_to_table, batch_size, epochs):
        self.epochs = epochs
        self.tokens = tokens
        self.classes = classes
        self.model_path = model_path
        self.table = pd.read_csv(path_to_table)  # table has to have the following columns: "length" (nmb of tokens in title),
        # "label_numerical" (label in number), "title"
        self.batch_size = batch_size
        self.epochs = epochs

    def tokenizer(self):
        return BertTokenizerFast.from_pretrained(self.model_path)

    def model(self):
        return BertForSequenceClassification.from_pretrained(self.model_path,
                                                             num_labels=self.classes,
                                                             output_attentions=False,
                                                             output_hidden_states=False)

    def tokenize(self):
        input_ids = []
        attention_masks = []
        for sentence in self.table['title'].tolist():
            encoded_dict = self.tokenizer.encode_plus(
                sentence,
                add_special_tokens=True,
                max_length=int(self.table.length.mean() + 2 * self.table.length.std()),
                pad_to_max_length=True,
                return_attention_mask=True,
                return_tensors='pt',
                truncation=True
            )
            input_ids.append(encoded_dict['input_ids'])
            attention_masks.append(encoded_dict['attention_mask'])
        input_ids = torch.cat(input_ids, dim=0)
        attention_masks = torch.cat(attention_masks, dim=0)
        labels = torch.tensor(self.table['label_numerical'].tolist())
        return input_ids, attention_masks, labels

    def make_datasets(self):
        """create PyTorch Datasets objects"""
        input_ids, attention_masks, labels = self.tokenize()
        dataset = TensorDataset(input_ids, attention_masks, labels)
        train_size = int(0.9 * len(dataset))
        val_size = len(dataset) - train_size
        train_dataset, val_dataset = random_split(dataset, [train_size, val_size])
        return train_dataset, val_dataset

    def make_dataloaders(self):
        train_dataset, val_dataset = self.make_datasets()
        train_dataloader = DataLoader(
            train_dataset,
            sampler=SequentialSampler(val_dataset),
            batch_size=self.batch_size
        )
        validation_dataloader = DataLoader(
            val_dataset,  # The validation samples.
            sampler=SequentialSampler(val_dataset),  # Pull out batches sequentially.
            batch_size=self.batch_size  # Evaluate with this batch size.
        )
        return train_dataloader, validation_dataloader

    def put_model_to_cuda(self):
        self.model().cuda()

    def get_total_train_steps(self):
        return self.epochs * len(self.make_dataloaders()[0])

    def make_optimizer(self):
        optimizer = AdamW(self.model().parameters(),
                          lr=2e-5,  # args.learning_rate - default is 5e-5, our notebook had 2e-5
                          eps=1e-8  # args.adam_epsilon  - default is 1e-8.
                          )
        return optimizer

    def make_scheduler(self):
        scheduler = get_linear_schedule_with_warmup(self.make_optimizer(),
                                                    num_warmup_steps=0,  # Default value in run_glue.py
                                                    num_training_steps=self.get_total_train_steps())






def prepare_data_for_classifier(table,
                                tokenizer,
                                model,
                                batch_size=32,
                                validation_split=0.1):
    """returns two objects of DataLoader class - train and validation splits"""
    input_ids = []
    attention_mask = []

    for sent in table.title.tolist():
        encoded_dict = tokenizer.encode_plus(sent,
                                             add_special_tokens=True,
                                             max_length=50,
                                             pad_to_max_length=True,
                                             return_attention_mask=True,
                                             return_tensors='pt',
                                             truncation=True)
    input_ids.append(encoded_dict['input_ids'])
    attention_mask.append(encoded_dict['attention_mask'])

    input_ids = torch.cat(input_ids, dim=0)
    attention_mask = torch.cat(attention_mask, dim=0)
    labels = torch.tensor(table.label_numerical.tolist())

    dataset = TensorDataset(input_ids, attention_mask, labels)

    # create a validation split

    train_size = int((1 - validation_split) * len(dataset))
    val_size = len(dataset - train_size)

    train_dataset, val_dataset = random_split(dataset, [train_size, val_size])
    train_dataloader = DataLoader(
        train_dataset,
        sampler=RandomSampler(train_dataset),
        batch_size=batch_size
    )

    validation_dataloader = DataLoader(
        val_dataset,
        sampler=SequentialSampler(val_dataset),
        batch_size=batch_size
    )

    return train_dataloader, validation_dataloader


def setup_training(model,
                   epochs=6
                   ):
    model.cuda()
    optimizer = AdamW(model.patameters(),
                      lr=2e-5,
                      eps=1e-8
                      )

    total_steps = len(train_dataloader)
    scheduler = get_linear_schedule_with_warmup(optimizer,
                                                num_warmup_steps=0,
                                                num_training_steps=total_steps)

    def flat_accuracy(preds, labels):
        pred_flat = np.argmax(preds, axis=1).flatten()
        labels_flat = labels.flatten()
        return np.sum(pred_flat == labels_flat) / len(labels_flat)

    seed_val = 42
    random.seed(seed_val)
    np.random.seed(seed_val)
    torch.manual_seed(seed_val)
    torch.cuda.manual_seed_all(seed_val)
    training_stats = []

    total_t0 = time.time()

    # train the model
    for epoch in range(epochs):
        print(f"Epoch {epoch / epochs}")
        total_train_loss = 0

        model.train()

        for step, batch in enumerate(train_dataloader):

            if step % 50 == 0 and not step == 0:
                print('  Batch {:>5,}  of  {:>5,}.    Elapsed: {:}.'.format(step, len(train_dataloader), elapsed))


if __name__ == '__main__':
    table_finanse = pd.read_csv('/mnt/sd1/nlp/intentclassification/merged_augmented_business_insider.csv')
