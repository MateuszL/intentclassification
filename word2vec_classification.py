import pandas as pd
import gensim
from utils import text_cleaner, number_of_unique_words_in_comments
import numpy as np
from sklearn.metrics import f1_score
from gensim.models import Word2Vec
from sklearn import svm
from sklearn.model_selection import train_test_split
""" two approaches - averaging all elements, padding with zeros to the length of the longest sentence """
""" as a classifier SVMs were used on top of the features """


def data_loader(path):
    table = pd.read_csv(path, usecols=['Meta Description 1', '2_level 1'], encoding='utf8')  # feature, label
    number_of_classes = table['2_level 1'].nunique()
    dict1 = dict(zip(table['2_level 1'].unique().tolist(), range(0, number_of_classes)))  # converts labels to numbers
    table['labels'] = table['2_level 1'].map(dict1, na_action='ignore')
    table = table.drop(columns=['2_level 1'])
    table.columns = ['feature', 'label']
    return table


def get_word_embedding(words, model):
    """

    :param words: list of strings to be word2vec-ed
    :param model:
    :return:
    """
    # words = words.split(" ")
    vectors = [model[w] for w in words]

    return vectors


def train_word2vec(list_of_words):
    """ training custom word2vec embeddings for the model"""
    model = Word2Vec([list_of_words], size=100, window=5, min_count=1, workers=4)
    # model.train()
    model.save("word2vec.model")


def get_word2vec(table_train, column, model, i, polish_stopwords):
    try:
        return model.wv[text_cleaner(table_train[f'{column}'][i].split(" "), polish_stopwords)]
    except KeyError:
        return np.zeros([1, 300])


def padd_shorter_sentences(table, maksimum_sentence=50, pad_with_zeros=True):

    """
    :param table: list of lists that is to be padded with 0, output of get_word2vec
    :param maksimum_sentence: 50
    :param pad_with_zeros: 
    :return: shape 50,100 times number of observations
    """
    zeros_to_be_padded = np.zeros([maksimum_sentence-table.shape[0], 300])
    return np.r_[table, zeros_to_be_padded]


if __name__ == '__main__':

    model = gensim.models.KeyedVectors.load_word2vec_format(
        '/home/mlewandowski/Downloads/klasyfikacja teksty/plwiki_20180420_300d.txt', binary=False)

    # data loader
    path_train = '/home/mlewandowski/Downloads/klasyfikacja teksty/tvn_zdrowie_train_data_classification_links_categories_3k.csv'
    path_test = '/home/mlewandowski/Downloads/klasyfikacja teksty/tvn_zdrowie_test_data_classification_links_categories_3k.csv'
    polish_stopwords = pd.read_csv('/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/intentclassification/polish_stopwords.txt',
                                                                   header=None, names=['words'], encoding='utf8')\
        .words.\
        to_list()

    table_train = data_loader(path_train)
    table_test = data_loader(path_test)
    # get unique words from a given column
    # list_of_words = number_of_unique_words_in_comments(table_train, 'feature', polish_stopwords)

    # train a model on these unique words
    # train_word2vec(list_of_words)

    # model = Word2Vec.load('word2vec.model')

    features_padded = []
    for i in range(table_train.shape[0]):
        features_padded.append(padd_shorter_sentences(get_word2vec(table_train, 'feature', model, i, polish_stopwords))
                               .reshape(-1,))


    X_train, X_test, y_train, y_test = train_test_split(pd.DataFrame(features_padded), table_train.label,
                                                        test_size=0.33,
                                                        random_state=42, shuffle=True)

    clf = svm.SVC(decision_function_shape='ovo')
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    score = f1_score(y_test, y_pred, average='micro')
    print(score)
    # with model trained on data
    # micro
    # 0.37676767676767675
    # None
    # [0.42732049 0.42857143 0.2745098  0.42307692 0.27777778 0.20833333
    #  0.         0.27272727 0.33333333 0.12903226 0.37209302 0.78787879
    #  0.20779221 0.         0.         0.12903226 0.5        0.
    #  0.         0.

    # for pretrained (with padding) 0.20202020202020202  - ? might have had too many 0's
    #


    # 0.3707070707070707 for averaging across all words





