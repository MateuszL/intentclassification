from pyMorfologik import Morfologik
from pyMorfologik.parsing import ListParser
import nltk
import pandas as pd
import string
import numpy as np


def text_cleaner(string2, polish_stopwords):

    from cleantext import clean
    cleaned_string = clean(string2, no_urls=True, no_digits=True, no_line_breaks=True, no_emails=True, no_phone_numbers=True,
                 no_numbers=True, no_currency_symbols=True, no_punct=True, replace_with_digit="", replace_with_url="",
                 replace_with_email="", replace_with_currency_symbol="", replace_with_number="",
                 replace_with_phone_number="")

    filtered_sentence = [elem for elem in cleaned_string.split(' ') if elem not in polish_stopwords]
    # optional - do not return it as a list of separate words, but as a string
    return filtered_sentence


def number_of_unique_words_in_comments(table, column, polish_stopwords, ret_uniq_words=True):
    """ returns either unique words in the whole colums or count of these unique words"""
    str1 = []
    for elem in table[f'{column}'].to_list():
        str1.extend(text_cleaner(elem, polish_stopwords))
    ilosc_elem = len(list(set(str1)))
    if ret_uniq_words:
        return list(set(str1))
    else:
        return ilosc_elem


def get_word_embedding(words, model):
    """
    :param words: string or list of strings, corresonding to each row of the function
    :param model:
    :return:
    """
    if not isinstance(words, list):
        words = words.split(" ")
    vectors = list()
    for w in words:
        try:
            vectors.append(model[w])
        except KeyError:
            vectors.append(np.zeros([1, 100]))

    return vectors


def reduce_dimensions(vector):
    """
    to be used with pandas .apply mthod, reduces dimensionality of a word embeddings
    :param vector: word embeddings
    :return: reduced, fixed size dimensionality
    """
    pass


def average_sequences(list_of_lists):
    return np.mean(list_of_lists, axis=0)


def data_loader(path):
    table = pd.read_csv(path, usecols=['Meta Description 1', '2_level 1'])  # feature, label
    # number_of_classes = table['2_level 1'].nunique()
    # dict1 = dict(zip(table['2_level 1'].unique().tolist(), range(0, number_of_classes)))  # converts labels to numbers
    # not needed for fast text
    # table['labels'] = table['2_level 1'].map(dict1, na_action='ignore')
    # table = table.drop(columns=['2_level 1'])
    table.columns = ['feature', 'label']
    return table


def get_prediction(vector, classifier):
    return classifier.predict(vector)


def check_if_a_word_is_in_string(string, word='ciąża'):
    """
    to be used in .apply to create a boolean column whether a given word is included in given string
    :param string: lemmatized
    :return:
    """
    if word in string.split(" "):
        return 1
    else:
        return 0


def get_prediciton_statistics():
    """set of useful lines, not very """
    column_predicted = ''
    columns_true = ''
    column_binary_word = ''
    table.query('is_word == 1 & label==Parenting')
    pass


def stem(string_to_stem, stopwords, parser, stemmer):
    """
    :param string_to_stem:  string
    :param stopwords: polish stopwords
    :param parser: ListParser() from pyMorfologik.parsing
    :param stemmer:  Morfologik() from pyMorfologik
    :return: list of stemmed strings
    """
    # remove digits
    remove_digits = str.maketrans('', '', string.digits)
    res = string_to_stem.translate(remove_digits)

    articles = nltk.word_tokenize(res, language='polish')
    articles = [elem.lower() for elem in articles if elem.lower() not in stopwords + list(string.punctuation)]
    nmb_of_tokens = len(articles)
    # input to stemmer should be string in a list
    articles = " ".join(articles)
    stemmed_list = []
    stemmed = stemmer.stem([articles], parser)  # returns a list of tuples, where second element in each tuple is a dict
    how_many = 0
    for i in range(nmb_of_tokens):
        try:
            if not [*stemmer.stem([articles], parser)[i][1]]:  # there is no stemmed version, take the original one
                stemmed_list.append(stemmer.stem([articles], parser)[i][0])
            else:  # there is a stemmed version, take the first possibility
                stemmed_list.append([*stemmer.stem([articles], parser)[i][1]][0])
        except IndexError:
            how_many += 1  # in case there are two words separated by a special symbol ('/') without a space between
            pass
    return " ".join(stemmed_list)



if __name__ == '__main__':
    parser = ListParser()
    stemmer = Morfologik()

    path_train = '/home/mlewandowski/Downloads/klasyfikacja teksty/tvn_zdrowie_train_data_classification_links_categories_3k.csv'
    path_test = '/home/mlewandowski/Downloads/klasyfikacja teksty/tvn_zdrowie_test_data_classification_links_categories_3k.csv'
    polish_stopwords = pd.read_csv('/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/intentclassification/polish_stopwords.txt',
                                                                   header=None, names=['words']).words.to_list()
    table_train = data_loader(path_train)
    table_test = data_loader(path_test)


    table_test['new_columns'] = table_test['feature'].apply(lambda x: stem(x, polish_stopwords, parser, stemmer))

    table_test.to_csv('/home/mlewandowski/Downloads/klasyfikacja teksty/stemmed_test_train.csv', index=False)


# table_train['is_word'] = table_train.new_columns.apply(lambda x: check_if_a_word_is_in_string(x))


def plotting_utils(clf, X_test, y_test):

    import numpy as np
    import matplotlib.pyplot as plt
    from sklearn.metrics import plot_confusion_matrix
    class_names = table_train.labels.unique()
    np.set_printoptions(precision=2)

    # Plot non-normalized confusion matrix
    titles_options = [("Confusion matrix, without normalization", None),
                      ("Normalized confusion matrix", 'true')]
    for title, normalize in titles_options:
        disp = plot_confusion_matrix(clf, X_test, y_test,
                                     display_labels=class_names,
                                     cmap=plt.cm.Blues,
                                     normalize=normalize)
        disp.ax_.set_title(title)

        print(title)
        print(disp.confusion_matrix)

    plt.show()
