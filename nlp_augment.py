from googletrans import Translator
import pandas as pd
import time
import httpx
import fasttext
import random
import nlpaug.augmenter.word as naw
import string


timeout = httpx.Timeout(60)
translator = Translator(timeout=timeout)


def translate(x):
    """translates polish to english and then back again from english to polish"""
    tl = translator.translate(x, src='pl')
    return translator.translate(tl.text, dest='pl').text


def replace_with_synonyms(x, ft, percent_to_change=0.5):
    """takes a pretrained model and replaces every word in undersampled classes with the word lying the closest, based
    on fasttext"""

    # ft.get_nearest_neighbors('cześć')

    how_many_words = round(len(x.split(" ")) * percent_to_change) + 1
    # get random indices of the words to change with a synonym
    synonyms_indices = random.sample(range(len(x.split(" "))), how_many_words)
    sentence = x.split(" ")
    for elem in synonyms_indices:
        try:
            sentence[elem] = ft.get_nearest_neighbors(sentence[elem])[0][1]
        except:
            sentence[elem] = sentence[elem]
            print("something went wrong, leaving the original element")
    return ' '.join(sentence)


def bert_replace(text):
    """this function takes in a text and augments it simulating an error resulting from a keyboard"""
    aug = naw.ContextualWordEmbsAug(model_path = 'dkleczek/bert-base-polish-cased-v1',
                                    action='substitute')
    augmented_text = aug.augment(text)
    return augmented_text


def fast_text_augment(x):
    """this is for trial purposes, checks the misspelling augmentation for fast text classification"""

    pass


def remove_stopwords(x, polish_stopwords):
    cleaned = [elem for elem in x if elem not in polish_stopwords]
    cleaned = [elem for elem in cleaned if elem not in string.punctuation]
    cleaned = [elem for elem in cleaned if not elem.isdigit()]
    return " ".join(cleaned)


if __name__ == '__main__':
    table_finanse = pd.read_csv('/mnt/sd1/nlp/intentclassification/BI_finanse.csv', usecols=['title', 'label'])

    # make a data augmentation based on there and back translation
    data_to_augment = table_finanse.loc[(table_finanse.label == 'sport') | (table_finanse.label == 'biznesvscovid') |
                            (table_finanse.label == 'wideo') | (table_finanse.label == 'strategie')]

    data_to_augment['augmented'] = data_to_augment.title.apply(lambda x: translate(x))
    data_to_augment.to_csv('/mnt/sd1/nlp/intentclassification/BI_finanse_augmented.csv')

    # synonym based augmentation
    # read the model
    path_to_model = '/mnt/sd1/pretrained_models/cc.pl.300.bin'
    ft = fasttext.load_model(path_to_model)
    # do the augmentation
    data_to_augment['augmented_synonyms'] = data_to_augment.title.\
        apply(lambda x: replace_with_synonyms(x, ft, percent_to_change=0.5))
    data_to_augment.to_csv('/mnt/sd1/nlp/intentclassification/BI_finanse_augmented_synonyms_222.csv')

    # now concatenate the augmented dataset on indexes and thus augment the dataset

    data_translation = pd.read_csv('/mnt/sd1/nlp/intentclassification/BI_finanse_augmented.csv')
    data_synonyms = pd.read_csv('/mnt/sd1/nlp/intentclassification/BI_finanse_augmented_synonyms.csv')

    # rename the columns for concatenation
    data_translation.columns = ['index', 'title', 'label', 'augmented']
    data_synonyms.columns = ['index', 'title', 'label', 'augmented']
    # all I care about is to have columns "label" and augmented added to the original dataframe
    new_table = pd.concat([data_synonyms[['augmented', 'label']],
                          data_translation[['augmented', 'label']]], axis=0)
    # write to csv
    new_table.columns = ['title', 'label']
    new_table_1 = pd.concat([new_table, table_finanse], axis=0)

    new_table_1.to_csv('/mnt/sd1/nlp/intentclassification/merged_augmented_business_insider.csv', index=False)

    # now there are more examples of this classes, so need to augment more classes
    table_finanse = pd.read_csv('/mnt/sd1/nlp/intentclassification/merged_augmented_business_insider.csv',
                                usecols=['title', 'label'])

    data_to_augment = table_finanse.loc[(table_finanse.label == 'sport') | (table_finanse.label == 'biznesvscovid') |
                                        (table_finanse.label == 'wideo') | (table_finanse.label == 'strategie') |
                                        (table_finanse.label == 'motoryzacja') |
                                        (table_finanse.label == 'polityka') |
                                        (table_finanse.label == 'media') |
                                        (table_finanse.label == 'lifestyle')

    ]


    data_to_augment['bert_aug'] = data_to_augment.title.apply(lambda x: (bert_replace(x)))
    # hand inspection of quality of augmented data is dubious, but whatever lets proceed with the trial

    bert_augmented = data_to_augment[['bert_aug', 'label']]
    bert_augmented.columns = ['title', 'label']

    original_data = data_to_augment[['title', 'label']]

    # concat

    bert_original_data = pd.concat([bert_augmented, original_data], axis=0)

    polish_stopwords = pd.read_csv('/home/erazer/PycharmProjects/jigsaw/nlp_utils/polish_stopwords.txt', header=None,
                                   names=['words']).words.to_list()


    data = pd.read_csv('/mnt/sd1/nlp/intentclassification/merged_augmented_ft_bert_bt_business_insider.csv')
    data = data.dropna()
    data['no_stopwords'] = data.title.apply(lambda x: remove_stopwords(x.split(' '), polish_stopwords))
