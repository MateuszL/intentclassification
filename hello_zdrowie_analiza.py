import pandas as pd
from nlp_augment import translate
import httpx
from googletrans import Translator


timeout = httpx.Timeout(60)
translator = Translator(timeout=timeout)

branza_medyczna = pd.read_csv('/mnt/sd1/branza_medyczna/branza_medyczna_przetworzone_pliki_hello_zdrowie_categories.csv')

# write the counts to csv to

branza_medyczna.second_element.value_counts().to_csv('/mnt/sd1/branza_medyczna/hello_zdrowie_licznosci.csv')

branza_medyczna_analysis = branza_medyczna[['Title 1', 'second_element']]
branza_medyczna_analysis.columns = ['title', 'label']

too_few_observations = ['Zdrowe emocje',
                        'Zdrowe jedzenie',
                        'Zdrowa aktywność',
                        '_Bez kategorii',
                        'Zdrowy organizm',
                        'Zdrowy styl życia']



labels_to_augment = ['Okres', 'Seks', 'Uroda', 'Macierzyństwo', 'Emocje']


data_to_augment = branza_medyczna_analysis.loc[
    (branza_medyczna_analysis.label == 'Okres') |
    (branza_medyczna_analysis.label == 'Seks') |
    (branza_medyczna_analysis.label == 'Uroda') |
    (branza_medyczna_analysis.label == 'Macierzyństwo') |
    (branza_medyczna_analysis.label == 'Emocje')
    ]

data_to_augment['translated'] = data_to_augment.title.apply(lambda x: translate(x))

data_to_augment.to_csv('/mnt/sd1/hello_zdrowie_translated.csv', index=False)

# now make fasttext substitution
data_bt = data_to_augment[['translated', 'label']]
data_bt.columns = ['title', 'label']
data_original = data_to_augment[['title', 'label']]

word_substitution_augment = pd.concat([data_bt, data_original], axis=0)

new_ft_augmented = word_substitution_augment[['ft_subst', 'label']]
new_ft_augmented.columns = ['title', 'label']

old = word_substitution_augment[['title', 'label']]
combined = pd.concat([new_ft_augmented, old], axis=0)

# select only these categories from branza medyczna



branza_medyczna_analysis = branza_medyczna_analysis.loc[
                                                        (branza_medyczna_analysis.label == 'Zdrowie') |
                                                        (branza_medyczna_analysis.label == 'Odżywianie') |
                                                        (branza_medyczna_analysis.label == 'Styl Życia')
]

analyze = pd.concat([combined, branza_medyczna_analysis], axis=0)

def label_to_numerical(table):
    num_elem = table.label.nunique()
    dict_ = dict(zip(table.label.unique().tolist(), range(0, num_elem)))
    table['label_numerical'] = table.label.map(dict_, na_action='ignore')
    return table

def check_lengths(x):
    return len(x.split(' '))

# make the data for classification

# ft augmented
ft_augmented = pd.read_csv('/mnt/sd1/branza_medyczna/hello_zdrowie/hello_zdrowie_fasttextaugmented.csv')
ft_augmented = ft_augmented[['ft_subst', 'label']]
ft_augmented.columns = ['title', 'label']


# translated
bt_translated = pd.read_csv('/mnt/sd1/branza_medyczna/hello_zdrowie/hello_zdrowie_translated.csv')
bt_translated = bt_translated[['translated', 'label']]
bt_translated.columns = ['title', 'label']

oryginalne = pd.read_csv('/mnt/sd1/branza_medyczna/hello_zdrowie/branza_medyczna_przetworzone_pliki_hello_zdrowie_categories.csv')
branza_medyczna = oryginalne.copy()
branza_medyczna_analysis = branza_medyczna[['Title 1', 'second_element']]
branza_medyczna_analysis.columns = ['title', 'label']

data_to_concat_oryginalne = branza_medyczna_analysis.loc[
    (branza_medyczna_analysis.label == 'Okres') |
    (branza_medyczna_analysis.label == 'Seks') |
    (branza_medyczna_analysis.label == 'Uroda') |
    (branza_medyczna_analysis.label == 'Macierzyństwo') |
    (branza_medyczna_analysis.label == 'Emocje') |
    (branza_medyczna_analysis.label == 'Styl Życia') |
    (branza_medyczna_analysis.label == 'Odżywianie') |
    (branza_medyczna_analysis.label == 'Zdrowie')
    ]

concated = pd.concat([data_to_concat_oryginalne,
                      bt_translated,
                      ft_augmented], axis=0)


concated = label_to_numerical(concated)

concated['length'] = concated.title.apply(check_lengths)
table_finanse = concated.copy()



