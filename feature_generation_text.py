import os
import numpy as np
import pandas as pd
import time
from tqdm import tqdm
import h2o

import lightgbm as lgb

from sklearn.metrics import f1_score
from sklearn.model_selection import KFold
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
from sklearn.naive_bayes import GaussianNB, MultinomialNB, BernoulliNB
from sklearn.utils import shuffle
import nltk
import string

from scipy.sparse import hstack

# import matplotlib.pyplot as plt
# import seaborn as sns
polish_stopwords = pd.read_csv('/home/erazer/PycharmProjects/jigsaw/nlp_utils/polish_stopwords.txt', header=None,
                               names=['words']).words.to_list()

table_finanse = pd.read_csv('/mnt/sd1/nlp/intentclassification/finanse_dane.csv', usecols=['new_cleaned',
                                                                                           'label_numerical']
                            )

# of these labels there is at most 6 observations, making it absolutely impossible to classify correctly
table_finanse = table_finanse[table_finanse.label_numerical != 16]
table_finanse = table_finanse[table_finanse.label_numerical != 15]
table_finanse = table_finanse[table_finanse.label_numerical != 17]
table_finanse = table_finanse[table_finanse.label_numerical != 18]
table_finanse = table_finanse[table_finanse.label_numerical != 19]

# shuffle it randomly
merged = shuffle(table_finanse)
# get rid of nans
merged = merged[merged.isna() == False]
train = merged.iloc[:int(merged.shape[0] * 0.8), :]
test = merged.iloc[int(merged.shape[0] * 0.8):, :]
#
# ## Number of words in the text ##
# train["num_words"] = train["article"].apply(lambda x: len(str(x).split()))
# test["num_words"] = test["article"].apply(lambda x: len(str(x).split()))
#
# ## Number of unique words in the text ##
# train["num_unique_words"] = train["article"].apply(lambda x: len(set(str(x).split())))
# test["num_unique_words"] = test["article"].apply(lambda x: len(set(str(x).split())))
#
# ## Number of characters in the text ##
# train["num_chars"] = train["article"].apply(lambda x: len(str(x)))
# test["num_chars"] = test["article"].apply(lambda x: len(str(x)))
#
# ## Number of stopwords in the text ##
# train["num_stopwords"] = train["article"].apply(lambda x: len([w for w in str(x).lower().split() if w in polish_stopwords]))
# test["num_stopwords"] = test["article"].apply(lambda x: len([w for w in str(x).lower().split() if w in polish_stopwords]))
#
# ## Number of punctuations in the text ##
# train["num_punctuations"] =train['article'].apply(lambda x: len([c for c in str(x) if c in string.punctuation]) )
# test["num_punctuations"] =test['article'].apply(lambda x: len([c for c in str(x) if c in string.punctuation]) )
#
# ## Number of title case words in the text ##
# train["num_words_upper"] = train["article"].apply(lambda x: len([w for w in str(x).split() if w.isupper()]))
# test["num_words_upper"] = test["article"].apply(lambda x: len([w for w in str(x).split() if w.isupper()]))
#
# ## Number of title case words in the text ##
# train["num_words_title"] = train["article"].apply(lambda x: len([w for w in str(x).split() if w.istitle()]))
# test["num_words_title"] = test["article"].apply(lambda x: len([w for w in str(x).split() if w.istitle()]))
#
# ## Average length of the words in the text ##
# train["mean_word_len"] = train["article"].apply(lambda x: np.mean([len(w) for w in str(x).split()]))
# test["mean_word_len"] = test["article"].apply(lambda x: np.mean([len(w) for w in str(x).split()]))
#
#
# eng_features = ['num_words', 'num_unique_words', 'num_chars',
#                 'num_stopwords', 'num_punctuations', 'num_words_upper',
#                 'num_words_title', 'mean_word_len']
#
# kf = KFold(n_splits=5, shuffle=True, random_state=43)
# test_pred = 0
# oof_pred = np.zeros([train.shape[0], ])
#
# x_test = test[eng_features].values

train_target = train['label_numerical'].values
#
# for i, (train_index, val_index) in tqdm(enumerate(kf.split(train))):
#     x_train = train[eng_features].iloc[train_index].values
#     x_val = train[eng_features].iloc[val_index].values
#     y_train = train_target[train_index]
#     y_val = train_target[val_index]
#     classifier = LogisticRegression(C=0.1)
#     classifier.fit(x_train, y_train)
#     val_preds = classifier.predict_proba(x_val)[:, 1]
#     preds = classifier.predict_proba(x_test)[:, 1]
#     test_pred += 0.2 * preds
#     oof_pred[val_index] = val_preds
#
#
# pred_train = (oof_pred > 0.5).astype(np.int)
# print(f1_score(train_target, pred_train, average='micro'))


train_text = train['new_cleaned']
test_text = test['new_cleaned']
all_text = pd.concat([train_text, test_text])
all_text = all_text[all_text.isna() == False]
train_text = train_text[train_text.isna() == False]
test_text = test_text[test_text.isna() == False]


word_vectorizer = TfidfVectorizer(
    sublinear_tf=True,
    strip_accents='unicode',
    analyzer='word',
    token_pattern=r'\w{1,}',
    stop_words='english',
    ngram_range=(1, 2),
    max_features=5000
)

word_vectorizer.fit(all_text)
train_word_features = word_vectorizer.transform(train_text)
test_word_features = word_vectorizer.transform(test_text)


kf = KFold(n_splits=5, shuffle=True, random_state=43)
test_pred_tf = 0
oof_pred_tf = np.zeros([train.shape[0], ])

for i, (train_index, val_index) in tqdm(enumerate(kf.split(train_text))):
    x_train, x_val = train_word_features[train_index, :], train_word_features[val_index, :]
    y_train, y_val = train_target[train_index], train_target[val_index]
    classifier = LogisticRegression(class_weight="balanced", C=0.5, solver='sag')
    classifier.fit(x_train, y_train)
    val_preds = classifier.predict_proba(x_val)[:, 1]
    preds = classifier.predict_proba(test_word_features)[:, 1]
    test_pred_tf += 0.2*preds
    oof_pred_tf[val_index] = val_preds


pred_train = (oof_pred_tf > 0.8).astype(np.int)
print(f1_score(train_target, pred_train, average='macro'))




