import pandas as pd
import string
import httpx
from googletrans import Translator
import random
import fasttext

timeout = httpx.Timeout(60)
translator = Translator(timeout=timeout)


def translate(x):
    """translates polish to english and then back again from english to polish"""
    tl = translator.translate(x, src='pl')
    return translator.translate(tl.text, dest='pl').text

table = pd.read_csv('/mnt/sd1/branza_medyczna/zdrowie_wprost/branza_medyczna_przetworzone_pliki_zdrowie_wprost_cat.csv')

def select_k(group, k=1):
    if len(group) > k:
        return group


def select_k_less_than(group, k=5000):
    if len(group) < k:
        return group
    else:
        return group.sample(k)


table_1 = table.groupby('url_4').apply(select_k).reset_index(drop=True)

table_1 = table_1.dropna()

translation_augment = table_1.groupby('url_4').apply(select_k_less_than).reset_index(drop=True)

translation_augment.url_4.value_counts()
translation_augment['title_translated'] = translation_augment['Title 1'].apply(lambda x: translate(str(x)))

translation_augment.to_csv('/mnt/sd1/branza_medyczna/zdrowie_wprost/translation_augmented.csv',
             index=False)

# take care of naming
table_2 = table_1[['url_4', 'Title 1']]
table_2.columns = ['label', 'title']

translation_augment_1 = translation_augment[['url_4', 'title_translated']]
translation_augment_1.columns = ['label', 'title']


table_concatenated = pd.concat([table_2, translation_augment_1], axis=0)


data_to_ft_augment = table_concatenated.groupby('label').apply(select_k_less_than).reset_index(drop=True)

def replace_with_synonyms(x, ft, percent_to_change=0.5):
    """takes a pretrained model and replaces every word in undersampled classes with the word lying the closest, based
    on fasttext"""

    # ft.get_nearest_neighbors('cześć')

    how_many_words = round(len(x.split(" ")) * percent_to_change) + 1
    # get random indices of the words to change with a synonym
    synonyms_indices = random.sample(range(len(x.split(" "))), how_many_words)
    sentence = x.split(" ")
    for elem in synonyms_indices:
        try:
            sentence[elem] = ft.get_nearest_neighbors(sentence[elem])[0][1]
        except:
            sentence[elem] = sentence[elem]
            print("something went wrong, leaving the original element")
    return ' '.join(sentence)


path_to_model = '/mnt/sd1/pretrained_models/cc.pl.300.bin'
ft = fasttext.load_model(path_to_model)
# do the augmentation
data_to_ft_augment['augmented_synonyms'] = data_to_ft_augment.title. \
    apply(lambda x: replace_with_synonyms(x, ft, percent_to_change=0.5))
data_to_ft_augment.to_csv('/mnt/sd1/branza_medyczna/zdrowie_wprost/ft_augmented.csv',
             index=False)

# now read all the data



def label_to_number(table):
    num_elements = table.label.nunique()
    dict_ = dict(zip(table.label.unique().tolist(), range(num_elements)))
    table['label_numerical'] = table.label.map(dict_)
    return table


new_table.to_csv('/mnt/sd1/branza_medyczna/zdrowie_wprost/data_to_classify.csv', index=False)



