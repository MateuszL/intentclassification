from transformers import DistilBertTokenizerFast, XLMTokenizer, RobertaModel
from transformers import TFDistilBertForSequenceClassification
import tensorflow as tf
import numpy as np
import tqdm
import pandas as pd

MAX_LEN = 50
BATCH = 32

test_split = pd.read_csv('/mnt/sd1/nlp/test_split_for_bi_finance.csv')
train_split = pd.read_csv('/mnt/sd1/nlp/train_split_for_bi_finance.csv')

train_sentences = train_split.title.to_list()
train_labels = train_split.label_numerical.to_list()
test_sentences = test_split.title.to_list()
test_labels = test_split.label_numerical.to_list()

tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-uncased')
model = TFDistilBertForSequenceClassification.from_pretrained('distilbert-base-uncased', num_labels=15)

# just in case
# tokenizer = XLMTokenizer.from_pretrained("allegro/herbert-klej-cased-tokenizer")
# model = RobertaModel.from_pretrained("allegro/herbert-klej-cased")


train_encodings = tokenizer(train_sentences,
                            truncation=True,
                            padding=True)
val_encodings = tokenizer(test_sentences,
                            truncation=True,
                            padding=True)

train_dataset = tf.data.Dataset.from_tensor_slices((
    dict(train_encodings),
    train_labels
))
val_dataset = tf.data.Dataset.from_tensor_slices((
    dict(val_encodings),
    test_labels
))

epochs = 4

# We classify two labels in this example. In case of multiclass
# classification, adjust num_labels value

optimizer = tf.keras.optimizers.Adam(learning_rate=5e-5)
model.compile(optimizer=optimizer, loss=model.compute_loss, metrics=['accuracy'])
model.fit(train_dataset.batch(BATCH),
          epochs=epochs,
          batch_size=BATCH,
          validation_data=val_dataset.batch(BATCH))


# save model
model.save_pretrained("/mnt/sd1/nlp/model_bert_finanse_herbert")

# load model
loaded_model = TFDistilBertForSequenceClassification.from_pretrained("/mnt/sd1/nlp/model_bert_finanse_herbert")


def make_predictions_using_trained_model(sentence, loaded_model, tokenizer):
    """outputs a prediction for each title """
    """
    validation_data - dataframe with y_val and X_val
    """
    predict_input = tokenizer.encode(sentence,
                                     truncation=True,
                                     padding=True,
                                     return_tensors="tf")
    tf_output = loaded_model.predict(predict_input)[0]
    tf_prediction = tf.nn.softmax(tf_output, axis=1).numpy()[0]
    y_predicted = np.argmax(tf_prediction)
    return y_predicted

# to write to pandas predictions made by a model
test_split['y_pred'] = test_split.title.apply(lambda x: make_predictions_using_trained_model(x, loaded_model, tokenizer))




