import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import Model
import transformers
from tqdm import tqdm
from sklearn.utils import shuffle
from transformers import TFBertModel, BertTokenizer, AutoTokenizer, XLMTokenizer, AutoModel, GPT2Tokenizer, TFGPT2Model



def fast_encode(texts, tokenizer, chunk_size=256, maxlen=512):

    tokenizer.enable_truncation(max_length=maxlen)
    tokenizer.enable_padding(max_length=maxlen)
    all_ids = []

    for i in tqdm(range(0, len(texts), chunk_size)):
        text_chunk = texts[i:i + chunk_size].tolist()
        encs = tokenizer.encode_batch(text_chunk)
        all_ids.extend([enc.ids for enc in encs])

    return np.array(all_ids)


def regular_encode(texts, tokenizer, maxlen=512):
    enc_di = tokenizer.batch_encode_plus(
        texts,
        return_attention_masks=False,
        return_token_type_ids=False,
        pad_to_max_length=True,
        max_length=maxlen
    )

    return np.array(enc_di['input_ids'])


def build_model(transformer, max_len=512):

    input_word_ids = Input(shape=(max_len,), dtype=tf.int32, name="input_word_ids")
    sequence_output = transformer(input_word_ids)[0]
    cls_token = sequence_output[:, 0, :]
    out = Dense(20, activation='softmax')(cls_token)

    model = Model(inputs=input_word_ids, outputs=out)
    model.compile(Adam(lr=1e-5), loss='categorical_crossentropy', metrics=['accuracy'])

    return model


if __name__ == '__main__':

    pretrained_weights = 'gpt2'
    # pretrained_weights = 'allegro/herbert-klej-cased-v1'

    tokenizer = GPT2Tokenizer.from_pretrained(pretrained_weights)
    tokenizer.pad_token = tokenizer.eos_token
    model = TFGPT2Model.from_pretrained(pretrained_weights)

    # tokenizer = AutoTokenizer.from_pretrained("allegro/herbert-klej-cased-v1")
    # model = AutoModel.from_pretrained("allegro/herbert-klej-cased-v1")

    AUTO = tf.data.experimental.AUTOTUNE

    EPOCHS = 10
    BATCH_SIZE = 16
    MAX_LEN = 192
    MODEL = 'gpt2'


    # merged = pd.read_csv("/home/mlewandowski/Downloads/klasyfikacja teksty/merged.csv")
    merged = pd.read_csv("merged.csv")
    merged = shuffle(merged)
    train_split = merged.iloc[:int(merged.shape[0] * 0.8), :]
    test_split = merged.iloc[int(merged.shape[0] * 0.8):, :]


    x_train = regular_encode(train_split.new_columns.values, tokenizer, maxlen=MAX_LEN)
    y_train = train_split.labels.values


    x_valid = regular_encode(test_split.new_columns.values, tokenizer, maxlen=MAX_LEN)
    y_valid = test_split.labels.values


    train_dataset = (
        tf.data.Dataset
        .from_tensor_slices((x_train, tf.keras.utils.to_categorical(y_train)))
        .repeat()
        .shuffle(2048)
        .batch(BATCH_SIZE)
        .prefetch(AUTO)
    )

    valid_dataset = (
        tf.data.Dataset
        .from_tensor_slices((x_valid, tf.keras.utils.to_categorical(y_valid)))
        .batch(BATCH_SIZE)
        .cache()
        .prefetch(AUTO)
    )


    transformer_layer = (
        transformers.TFGPT2Model
            .from_pretrained('gpt2')
    )

    model = build_model(transformer_layer, max_len=MAX_LEN)
    model.summary()

    EPOCHS = 10
    n_steps = x_train.shape[0] // BATCH_SIZE
    train_history = model.fit(
        train_dataset,
        steps_per_epoch=n_steps,
        validation_data=valid_dataset,
        epochs=EPOCHS
    )
    model.save('model_gpt2.model')



