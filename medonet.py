import pandas as pd
import string
import httpx
from googletrans import Translator
import random
import fasttext
import pandarallel


timeout = httpx.Timeout(60)
translator = Translator(timeout=timeout)

# has mistakes with numbers ==> eliminate numbers first
def translate(x):
    """translates polish to english and then back again from english to polish"""
    try:
        tl = translator.translate(x, src='pl')
        return translator.translate(tl.text, dest='pl').text
    except:
        print(f'some mistaking while translation occured here {x}')
        return x


def select_k(group, k=54):
    if len(group) > k:
        return group


def select_k_less_than(group, k=1000):
    if len(group) < k:
        return group
    else:
        return group.sample(k)


def eliminate_spaces_from_label_names(x):
    return x.replace(" ", '_')

path = '/mnt/sd1/branza_medyczna/medonet/branza_medyczna_przetworzone_pliki_medonet_cat.csv'

table = pd.read_csv(path)

table_more_than_one_unique = table.groupby('bread_2').apply(select_k).reset_index(drop=True)

# columns naming
table_more_than_one_unique = table_more_than_one_unique[['bread_2', 'Title 1']]
table_more_than_one_unique.columns = ['label', 'title']
table_more_than_one_unique['new_label'] = table_more_than_one_unique.label.apply(lambda x: eliminate_spaces_from_label_names(str(x)))
table_more_than_one_unique = table_more_than_one_unique.dropna()


def check_if_pl(x):
    return str(x).endswith('.pl')

# get rid of links
table_more_than_one_unique['is_link'] = table_more_than_one_unique.title.apply(check_if_pl)

table_more_than_one_unique = table_more_than_one_unique.drop(table_more_than_one_unique[table_more_than_one_unique.is_link==True].index).reset_index(drop=True)



data_to_translation_augment = pd.read_csv('/mnt/sd1/branza_medyczna/medonet/data_to_translation_augment.csv')
data_to_translation_augment = table_more_than_one_unique.groupby('label').apply(select_k_less_than).reset_index(drop=True)

data_to_translation_augment['translation_augmented'] = data_to_translation_augment.title.apply(lambda x: translate(str(x)))
data_to_translation_augment.to_csv('/mnt/sd1/branza_medyczna/medonet/translation_augmented.csv')

# concat the table

data_to_translation_augment_1 = data_to_translation_augment[['label', 'translation_augmented']]
data_to_translation_augment_1.columns = ['label', 'title']

for_ft_augment = pd.concat([table_more_than_one_unique, data_to_translation_augment_1], axis=0)

# ft augment

def replace_with_synonyms(x, ft, percent_to_change=0.5):
    """takes a pretrained model and replaces every word in undersampled classes with the word lying the closest, based
    on fasttext"""

    # ft.get_nearest_neighbors('cześć')

    how_many_words = round(len(x.split(" ")) * percent_to_change) + 1
    # get random indices of the words to change with a synonym
    synonyms_indices = random.sample(range(len(x.split(" "))), how_many_words)
    sentence = x.split(" ")
    for elem in synonyms_indices:
        try:
            sentence[elem] = ft.get_nearest_neighbors(sentence[elem])[0][1]
        except:
            sentence[elem] = sentence[elem]
            print("something went wrong, leaving the original element")
    return ' '.join(sentence)

from pandarallel import pandarallel
pandarallel.initialize()

path_to_model = '/mnt/sd1/pretrained_models/cc.pl.300.bin'
ft = fasttext.load_model(path_to_model)
# do the augmentation
data_to_translation_augment['augmented_synonyms'] = data_to_translation_augment.title.\
    parallel_apply(lambda x: replace_with_synonyms(x, ft, percent_to_change=0.5))


def check_length(x):
    return len(str(x).split(' '))

table_finanse = table_more_than_one_unique.copy()
table_finanse['length'] = table_finanse.title.apply(check_length)

table_finanse = table_finanse.query('length!=1')

def cut_too_many(group, how_many=220):
    if len(group) < how_many:
        return group
    else:
        return group.sample(how_many)


balanced = table_finanse.groupby('new_label').apply(cut_too_many)

table_finanse['final_label'] = table_finanse.new_label.apply(lambda x: append_prefix(x))
table_string_label_stemmmed = table_finanse.drop(columns=['label', 'is_link', 'length', 'new_label'])

# merge label and title to one column
table_string_label_stemmmed["column_merged"] = table_string_label_stemmmed.apply(lambda x: ' '.join(x.astype(str)),
                                                                                 axis=1)

# make a train test split
merged = shuffle(table_string_label_stemmmed["column_merged"])
train_split = merged.iloc[:int(merged.shape[0] * 0.8)]
test_split = merged.iloc[int(merged.shape[0] * 0.8):]

# write it on a drive to get rid of index and header for fasttext classifier
train_split.to_csv('/mnt/sd1/branza_medyczna/medonet/table_train', index=False, header=None)
test_split.to_csv('/mnt/sd1/branza_medyczna/medonet/table_test', index=False, header=None)

# as a path use paths resulting from the above version
model = fasttext.train_supervised(input='/mnt/sd1/branza_medyczna/medonet/table_test',
                                  autotuneValidationFile='/mnt/sd1/branza_medyczna/medonet/table_test')

print(model.test('/mnt/sd1/branza_medyczna/medonet/table_test'))


sum([int(model.predict(table_finanse.title[i]) == table_finanse.label[i]) for i in range(table_finanse.shape[0])]), table_finanse.shape[0]


# take care og augmented dataset
table_finanse = pd.read_csv('/mnt/sd1/branza_medyczna/medonet/translation_augmented_max220.csv')
table_augmented = table_finanse[['label', 'translated_title']]
table_augmented.columns = ['label', 'title']

table_original = table_finanse[['label', 'title']]
new_table = pd.concat([table_augmented, table_original], axis=0)

table_finanse = new_table.copy()

balanced = table_finanse.groupby('label').apply(select_k_less_than).reset_index(drop=True)
table_finanse = balanced.copy()

def find_l(x):
    return len(x.split(' '))
table_finanse['length'] = table_finanse.title.apply(find_l)

def label_to_number(table):
    num_elements = table.label.nunique()
    dict_ = dict(zip(table.label.unique().tolist(), range(num_elements)))
    table['label_numerical'] = table.label.map(dict_)
    return table

table_finanse = label_to_number(table_finanse)



'/mnt/sd1/learn_to_rank_dataset/Gov/Feature_min/2003_hp_dataset/Fold1/test.txt'
