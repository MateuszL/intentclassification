import pandas as pd
from utils import text_cleaner, number_of_unique_words_in_comments
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.metrics import f1_score


class Doc:

    def __init__(self, path, polish_stopwords, path_to_save_table, path_to_save_model):
        self.path = path
        self.polish_stopwords = polish_stopwords
        self.path_to_save_table = path_to_save_table
        self.path_to_save_model = path_to_save_model

    def data_loader(self):
        self.table = pd.read_csv(self.path, usecols=['Meta Description 1', '2_level 1'], encoding='utf8')  # feature, label
        number_of_classes = self.table['2_level 1'].nunique()
        dict1 = dict(
            zip(self.table['2_level 1'].unique().tolist(), range(0, number_of_classes)))  # converts labels to numbers
        self.table['labels'] = self.table['2_level 1'].map(dict1, na_action='ignore')
        self.table = self.table.drop(columns=['2_level 1'])
        self.table.columns = ['feature', 'label']

    def data_cleaner(self):
        # self.data_loader()
        something = []
        for i in range(self.table.shape[0]):
            something.append((text_cleaner(self.table['feature'][i], self.polish_stopwords)))
        self.table['cleaned_sentences'] = pd.Series(something)
        return self.table

    def make_doc2_vec_model(self):
        # TODO TaggedDocument integer should be the order of a sentence in the document, not a label
        # self.data_cleaner()
        documents = [TaggedDocument(doc, [i]) for doc, i in zip(self.table['cleaned_sentences'], self.table.label)]
        self.model = Doc2Vec(documents=documents, size=100, min_count=1, dm_concat=1, )
        self.model.save(self.path_to_save_model)

    def return_inferred_vector(self):
        """

        :param i: element of a table
        :return: senteces (list of cleaned strings, embedded into a vector of shape (n,), n - size defined in method
        make_doc2_vec_model
        """
        # self.make_doc2_vec_model()
        # self.data_cleaner()
        self.model = Doc2Vec.load(self.path_to_save_model)
        self.sentence_embedded = []
        for i in range(self.table.shape[0]):
            self.sentence_embedded.append(self.model.infer_vector(text_cleaner(self.table['cleaned_sentences'][i], self.polish_stopwords)).reshape(-1,))
        # self.table['sentence_embedded'] = pd.Series(sentence_embedded)
        # self.table.to_csv(self.path_to_save_table, index=False)


    def run_classifier(self):
        X_train, X_test, y_train, y_test = train_test_split(self.sentence_embedded,
                                                            self.table['label'], test_size=0.33, random_state=42)

        clf = svm.SVC(decision_function_shape='ovo')
        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        score = f1_score(y_test, y_pred, average='micro')
        print(score)  # 0.5313131313131313



if __name__ == '__main__':

    path_train = '/home/mlewandowski/Downloads/klasyfikacja teksty/tvn_zdrowie_train_data_classification_links_categories_3k.csv'
    path_test = '/home/mlewandowski/Downloads/klasyfikacja teksty/tvn_zdrowie_test_data_classification_links_categories_3k.csv'
    polish_stopwords = pd.read_csv('/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/intentclassification/polish_stopwords.txt',
                                                                   header=None, names=['words']).words.to_list()
    # path_to_save_model = '/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/intentclassification/model_doc2vec.model'
    path_to_save_table = '/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/intentclassification/table_cleaned.csv'
    path_to_save_model = '/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/intentclassification/model_doc2vec.model'
    doc = Doc(path=path_train, polish_stopwords=polish_stopwords, path_to_save_table=path_to_save_table,
              path_to_save_model=path_to_save_model)
    doc.data_loader()
    doc.data_cleaner()
    doc.make_doc2_vec_model()
    doc.return_inferred_vector()
    doc.run_classifier()




