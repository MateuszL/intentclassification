import torch
from tokenizers import SentencePieceBPETokenizer
from tokenizers.processors import RobertaProcessing
from transformers import RobertaModel, AutoModel
import pandas as pd
import numpy as np


def encode_sentences(x, tokenizer, model):
    # outputs a torch class object, to get it to numpy use .detach().numpy()
    input = tokenizer.encode(x)
    output = model(torch.tensor([input.ids]))[0]
    return output[0][1].detach().numpy()


def perform_operation_in_batches(table, column, function, model, tokenizer, BATCH_SIZE=100):
    """
    this function serves performing some specific operation on a column in batches so to avoid RAM overload
    :param table: pd df
    :return: pd df with a column that has operation performed on it
    :column: what column are we operating on
    :function: what function are we applying on this columns
    """

    list_train = [table[f'{column}'].apply(lambda x: x)[i:i + BATCH_SIZE].to_list() for i in
                  range(0, table[f'{column}'].shape[0], BATCH_SIZE)]
    unloop_nested_list = [list_train[x] for x in range(len(list_train))]
    elmo_train = [function(x, model, tokenizer).detach().numpy() for x in unloop_nested_list]
    # return np.concatenate(elmo_train, axis=0).reshape([table.shape[0], -1])
    return np.concatenate(elmo_train, axis=0)


if __name__ == '__main__':
    # model_dir = '/home/mlewandowski/Downloads/pretrained_models/roberta_base_transformers'
    # stemmed_cleaned_dataset_path = '/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/intentclassification/merged.csv'
    model_dir = '/mnt/sd1/roberta_base_transformers'
    stemmed_cleaned_dataset_path = '/mnt/sd1/nlp/intentclassification/merged.csv'



    tokenizer = SentencePieceBPETokenizer(f"{model_dir}/vocab.json", f"{model_dir}/merges.txt")
    getattr(tokenizer, "_tokenizer").post_processor = RobertaProcessing(sep=("</s>", 2), cls=("<s>", 0))
    model: RobertaModel = AutoModel.from_pretrained(model_dir)

    table = pd.read_csv(stemmed_cleaned_dataset_path)
    # roberta = perform_operation_in_batches(table, 'new_columns', encode_sentences, tokenizer, model)
    roberta = table.new_columns.apply(lambda x: encode_sentences(x, tokenizer, model))
    np.save('roberta.npy', roberta.to_list())


    roberta_labels = pd.read_csv(stemmed_cleaned_dataset_path).labels
    from sklearn.svm import SVC
    from sklearn.metrics import f1_score
    clf = SVC(kernel='linear')

    from sklearn.model_selection import train_test_split
    from sklearn.metrics import f1_score

    X_train, X_test, y_train, y_test = train_test_split(roberta, roberta_labels, test_size=0.33, random_state=42)
    clf.fit(X_train,y_train)
    predicted = clf.predict(X_test)
    f1 = f1_score(y_test,predicted,average='micro')



