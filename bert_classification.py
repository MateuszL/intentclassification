import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import Model
import transformers
from tqdm import tqdm
from sklearn.utils import shuffle
from transformers import TFBertModel, BertTokenizer, AutoTokenizer, XLMTokenizer, AutoModel
import tensorflow_addons as tfa


def fast_encode(texts, tokenizer, chunk_size=256, maxlen=512):

    tokenizer.enable_truncation(max_length=maxlen)
    tokenizer.enable_padding(max_length=maxlen)
    all_ids = []

    for i in tqdm(range(0, len(texts), chunk_size)):
        text_chunk = texts[i:i + chunk_size].tolist()
        encs = tokenizer.encode_batch(text_chunk)
        all_ids.extend([enc.ids for enc in encs])

    return np.array(all_ids)


def regular_encode(texts, tokenizer, maxlen=512):
    enc_di = tokenizer.batch_encode_plus(
        texts,
        return_attention_mask=False,
        return_token_type_ids=False,
        pad_to_max_length=True,
        max_length=maxlen,
        truncation=True
    )

    return np.array(enc_di['input_ids'])


def build_model(transformer, max_len=512):

    input_word_ids = Input(shape=(max_len,), dtype=tf.int32, name="input_word_ids")
    sequence_output = transformer(input_word_ids)[0]
    cls_token = sequence_output[:, 0, :]
    out = Dense(15, activation='softmax')(cls_token)
    # model = transformer.distilbert(inputs=input_word_ids, outputs=out)
    model = Model(inputs=input_word_ids, outputs=out)
    model.compile(Adam(lr=1e-5), loss='categorical_crossentropy', metrics=['accuracy'])

    return model




if __name__ == '__main__':

    pretrained_weights = 'bert-base-uncased'
    # pretrained_weights = 'allegro/herbert-klej-cased-v1'

    tokenizer = BertTokenizer.from_pretrained(pretrained_weights)
    model = TFBertModel.from_pretrained(pretrained_weights)

    # tokenizer = AutoTokenizer.from_pretrained("allegro/herbert-klej-cased-v1")
    # model = AutoModel.from_pretrained("allegro/herbert-klej-cased-v1")

    AUTO = tf.data.experimental.AUTOTUNE


    BATCH_SIZE = 32
    MAX_LEN = 50
    MODEL = 'jplu/tf-xlm-roberta-large'

    # merged = pd.read_csv("/mnt/sd1/nlp/intentclassification/merged.csv")  # for medical data

    # table_finanse = pd.read_csv('/mnt/sd1/nlp/intentclassification/BI_finanse.csv', usecols=['title', 'label_numerical'])
    #
    # merged = shuffle(table_finanse, random_state=42)
    # train_split = merged.iloc[:int(merged.shape[0] * 0.8), :]
    # test_split = merged.iloc[int(merged.shape[0] * 0.8):, :]
    test_split = pd.read_csv('/mnt/sd1/nlp/test_split_for_bi_finance.csv')
    train_split = pd.read_csv('/mnt/sd1/nlp/train_split_for_bi_finance.csv')

    x_train = regular_encode(train_split.title.values, tokenizer, maxlen=MAX_LEN)
    y_train = train_split.label_numerical.values

    x_valid = regular_encode(test_split.title.values, tokenizer, maxlen=MAX_LEN)
    y_valid = test_split.label_numerical.values


    train_dataset = (
        tf.data.Dataset
        .from_tensor_slices((x_train, tf.keras.utils.to_categorical(y_train, num_classes=15)))
        .repeat()
        .shuffle(2048)
        .batch(BATCH_SIZE)
        .prefetch(AUTO)
    )

    valid_dataset = (
        tf.data.Dataset
        .from_tensor_slices((x_valid, tf.keras.utils.to_categorical(y_valid, num_classes=15)))
        .batch(BATCH_SIZE)
        .cache()
        .prefetch(AUTO)
    )


    transformer_layer = (
        transformers.TFDistilBertModel
            .from_pretrained('distilbert-base-multilingual-cased')
    )

    model = build_model(transformer_layer, max_len=MAX_LEN)
    model.summary()

    EPOCHS = 1
    n_steps = x_train.shape[0] // BATCH_SIZE
    early_stopping = tf.keras.callbacks.EarlyStopping(
        monitor='val_auc',
        verbose=1,
        patience=3,
        mode='max',
        restore_best_weights=True)


    # This function keeps the initial learning rate for the first ten epochs
    # and decreases it exponentially after that.

    def scheduler(epoch, lr):
        if epoch < 3:
            return lr
        else:
            return lr * tf.math.exp(-0.1)

    lr_schedule = tf.keras.callbacks.LearningRateScheduler(scheduler)

    train_history = model.fit(
        train_dataset,
        steps_per_epoch=n_steps,
        validation_data=valid_dataset,
        epochs=EPOCHS,
        # callbacks=[early_stopping, lr_schedule]
    )
    model.save('/mnt/sd1/nlp/model_bert_finanse_callbacks')
    # tf.saved_model.save(model, '/mnt/sd1/nlp/model_bert_finanse_callbacks')

    # now load the model for predictions





