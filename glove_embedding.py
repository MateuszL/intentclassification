from gensim.models import KeyedVectors
import pandas as pd
import gensim
import numpy as np
from sklearn.metrics import f1_score
from gensim.models import Word2Vec
from sklearn import svm
from sklearn.model_selection import train_test_split
from utils import average_sequences


def labels_to_numbers(table):
    number_of_classes = table['label'].nunique()
    dict1 = dict(zip(table['label'].unique().tolist(), range(0, number_of_classes)))  # converts labels to numbers
    table['label_numerical'] = table['label'].map(dict1, na_action='ignore')
    return table


def get_word_embedding(words, model):
    """

    :param words: list of strings or string to be word2vec-ed
    :param model:
    :return:
    """
    if not isinstance(words, list):
        words = words.split(" ")
    vectors = list()
    for w in words:
        try:
            vectors.append(model[w])
        except KeyError:
            vectors.append(np.zeros([1, 100])[0])  # somehow np.zeros([1,100]) makes

    return vectors


def padd_shorter_sentences(table, maksimum_sentence=50, pad_with_zeros=True):

    """
    :param table: list of lists that is to be padded with 0, output of get_word2vec
    :param maksimum_sentence: 50
    :param pad_with_zeros:
    :return: shape 50,100 times number of observations
    """
    table = pd.DataFrame(table)
    zeros_to_be_padded = np.zeros([maksimum_sentence-table.shape[0], 100])
    return np.r_[table, zeros_to_be_padded]




if __name__ == '__main__':
    # choose a model that you want an embedding from
    # glove
    model_glove = KeyedVectors.load_word2vec_format("/home/mlewandowski/Downloads/pretrained_models/glove_100_3_polish.txt")
    # word2vec
    model_word2vec = KeyedVectors.load('/home/mlewandowski/Downloads/pretrained_models/word2vec/word2vec_100_3_polish.bin')
    # fasttext
    model_fasttext = KeyedVectors.load('/home/mlewandowski/Downloads/pretrained_models/fasttext_v2/fasttext_100_3_polish.bin')

    path_train_stemmed = '/home/mlewandowski/Downloads/klasyfikacja teksty/stemmed_text_train.csv'
    path_test_stemmed = '/home/mlewandowski/Downloads/klasyfikacja teksty/stemmed_test_train.csv'

    path_merged = '/home/mlewandowski/Downloads/klasyfikacja teksty/merged.csv'

    table_train = pd.read_csv(path_merged)
    table_test = pd.read_csv(path_test_stemmed)


    table_train = labels_to_numbers(table_train)
    table_train['new'] = table_train.new_columns.apply(lambda x: get_word_embedding(x, model_word2vec))
    table_train['padded'] = table_train.new.apply(lambda x: padd_shorter_sentences(x))
    table_train['padded_reshape'] = table_train.padded.apply(lambda x: x.reshape(-1,))
    # optional to avoid padding
    table_train['averaged'] = table_train.new.apply(lambda x: average_sequences(x))

    # for test data
    table_test = labels_to_numbers(table_test)
    table_test['new'] = table_test.new_columns.apply(lambda x: get_word_embedding(x, model_word2vec))
    table_test['padded'] = table_test.new.apply(lambda x: padd_shorter_sentences(x))  # how do I know the longest sequence?
    table_test['padded_reshape'] = table_test.padded.apply(lambda x: x.reshape(-1,))
    # optional to avoid padding
    table_test['averaged'] = table_test.new.apply(lambda x: average_sequences(x))

    # testing and training on the same datset (with train test split)
    X_train, X_test, y_train, y_test = train_test_split(pd.DataFrame(table_train['padded_reshape'].tolist()), table_train.labels,
    test_size=0.33, random_state=42, shuffle=True)

    # testing and training on different dataset

    # X_train = pd.DataFrame(table_train['padded_reshape'].tolist())
    # X_test = pd.DataFrame(table_test['padded_reshape'].tolist())
    # y_train = table_train.labels
    # y_test = table_test.labels


    # # optional to see whether concatenating solves the problem of absolute randomness
    # merged_train = pd.DataFrame(pd.concat([table_train['padded_reshape'], table_test['padded_reshape']], axis=0).tolist())
    # merged_labels = pd.DataFrame(pd.concat([table_train.labels, table_test.labels], axis=0).tolist())
    #
    # X_train, X_test, y_train, y_test = train_test_split(merged_train, merged_labels,
    #                                                     test_size=0.33, random_state=42, shuffle=True)




    clf = svm.SVC(decision_function_shape='ovo')
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    # optional to avoid padding
    score = f1_score(y_test, y_pred, average='micro')
    print(score)




