import pandas as pd
import string
import httpx
from googletrans import Translator
import random
import fasttext
import pandarallel

table = pd.read_csv('/mnt/sd1/nlp/intentclassification/merged_uncleaned.csv')


table.columns=['title', 'label']
# first try to ct the value counts to 500, then see what happens

def cut_values(group, k=500):
    if len(group) < k:
        return group
    else:
        return group.sample(k)


balanced = table.groupby('label').apply(cut_values).reset_index(drop=True)

def label_to_number(table):
    how_many = table.label.nunique()
    dict_ = dict(zip(table.label.unique().tolist(), range(how_many)))
    table['label_numerical'] = table.label.map(dict_)
    return table


balanced = label_to_number(balanced)

def get_length(x):
    return len(x.split(' '))

balanced['length'] = balanced.title.apply(get_length)
table_finanse = balanced.copy()


#for plots

accuracy = [0.65, 0.72, 0.78, 0.84, 0.88, 0.90, 0.91, 0.92]
validation_accuracy = [0.61, 0.68, 0.72, 0.82, 0.86, 0.86, 0.87, 0.87]
train_loss = [1.72, 0.83, 0.46, 0.26, 0.15, 0.1, 0.07, .05]
validation_loss = [1.06, 0.75, 0.65, 0.58, 0.63, 0.59, 0.6, .6]
epoch = range(1,8)

accuracy = [0.6, 0.71, 0.79, 0.8]
validation_accuracy = [0.55, 0.65, 0.71, 0.72]
# train_loss = [1.64, 0.84, 0.46, 0.34]
# validation_loss = [1.4, 1.1, 0.58, 0.53]

import numpy as np
x = np.linspace(1, 4, 4)
import matplotlib.pyplot as plt
plt.plot(x, accuracy)
plt.plot(x, validation_accuracy)
plt.legend(['train_F1', 'validation_F1'], loc='upper left')
plt.ylabel('F1_macro')
plt.xlabel('epoch')
plt.show()

