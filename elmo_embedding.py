import tensorflow as tf
import pandas as pd
import tensorflow_hub as hub
from sklearn.utils import shuffle
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import f1_score
import xgboost
from allennlp.modules.elmo import Elmo, batch_to_ids
from sklearn.decomposition import PCA

# table = pd.read_csv('/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/intentclassification/merged.csv')
# elmo = hub.Module("https://tfhub.dev/google/elmo/2", trainable=True)




def elmo_vectors(x):
    """
    this function returns english elmo embeddings
    :param x
    :return:
    """

    embeddings = hub.elmo(x, signature="default", as_dict=True)["elmo"]

    with tf.Session() as sess:
      sess.run(tf.global_variables_initializer())
      sess.run(tf.tables_initializer())
      return sess.run(tf.reduce_mean(embeddings, 1))
      # return sess.run(embeddings)

# then load batches of text
# encode them into size of 100 batches

# test = np.load('/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/elmo_test_new.npy')
# train = np.load('/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/elmo_train_new.npy')
# train_labels = np.load('/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/labels_train.npy')
# test_labels = np.load('/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/labels_test.npy')

#
# logreg = LogisticRegression()
# logreg.fit(train, train_labels)
# y_pred = logreg.predict(test)
# f1_score(test_labels, y_pred, average='macro')  # 0.34485961081709754


def make_polish_elmo_embedding(x):

    options_file = "/home/mlewandowski/Downloads/pretrained_models/elmo_polish/elmo_polish_info.json"
    weights_file = "/home/mlewandowski/Downloads/pretrained_models/elmo_polish/elmo_polish_weights.hdf5"

    elmo = Elmo(options_file, weights_file, 2, dropout=0)

    # sentences = [['To', 'jest', 'tekst', '.'], ['Kolejny', 'tekst', '.']]
    character_ids = batch_to_ids(x)
    embeddings = elmo(character_ids)
    return embeddings['elmo_representations'][0].mean(dim=1)


def split_string(x):
    return x.split(" ")


def padd_shorter_sequences(x, max_length):
    """

    :param x: observation in pandas
    :param max_length: length of longest sequence
    :return: same size length of sentences
    """
    pass


if __name__ == '__main__':

    table = pd.read_csv('/home/mlewandowski/PycharmProjects/klasyfikacja_tekstu/intentclassification/merged.csv')

    # for english based elmo
    # elmo = hub.Module("https://tfhub.dev/google/elmo/2", trainable=True)

    merged = shuffle(table)
    # train_split = merged.iloc[:10]
    train_split = merged.iloc[:int(merged.shape[0] * 0.8)]
    test_split = merged.iloc[int(merged.shape[0] * 0.8):]

    BATCH_SIZE = 100
    list_train = [train_split.new_columns.apply(lambda x: split_string(x)).to_list()[i:i + 100] for i in range(0, train_split.new_columns.shape[0], 100)]
    list_test = [test_split.new_columns.apply(lambda x: split_string(x)).to_list()[i:i + 100] for i in range(0, test_split.new_columns.shape[0], 100)]

    elmo_train = [make_polish_elmo_embedding(x).detach().numpy() for x in list_train]
    elmo_test = [make_polish_elmo_embedding(x).detach().numpy() for x in list_test]

    elmo_polish_train_new = np.concatenate(elmo_train, axis=0)
    elmo_polish_test_new = np.concatenate(elmo_test, axis=0)
    np.save('elmo_polish_train_new.npy', elmo_polish_train_new)
    np.save('elmo_polish_test_new.npy', elmo_polish_test_new)

    # classify wth svm micro 0.6374613872507723, macro - 0.5486935814263398
    clf = SVC()
    clf.fit(elmo_polish_train_new, labels_polish_elmo_train)
    predictions = clf.predict(elmo_polish_test_new)
    f1_score(labels_polish_elmo_test, predictions, average='micro')
    f1_score(labels_polish_elmo_test, predictions, average='macro')



